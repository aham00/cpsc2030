 <?php
 
session_start();
if (!isset($_SESSION['username'])) {
    header('location:../login.php');
}
include_once'../layout/page.php';
include_once'../conn.php';
    head("../Stylesheet/style.css","Users Auctions");
    top();
    nav();

$dt = $_SESSION['username'].'Bids';
?>
<form action="UserDeleteAuction.php" method="POST">
    <input type="text" name="id" placeholder="Auction ID #">
    <br>
    <button type="submit" name="submit">Delete Auction</button>
</form>
<?php

    
        $sql = "SELECT * FROM $dt;";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        echo "<div class='flex_cont'> ";
        
        if($resultCheck > 0){
            while($row = mysqli_fetch_assoc($result)){
                $id = $row['mech_id'];
                $brand = $row['brand'];
                $pc = $row['primary_colour'];
                $sc = $row['secondary_colour'];
                $pp = $row['power_plant'];
                $arm = $row['armaments'];
                $sArm = $row['special_armaments'];
                $sSF = $row['special_systems_features'];
                $scale = $row['scale'];
                
                echo "<div class='bidbox'>"
                    ."Auction ID #: ".$id."<br>"
                    ."Brand: ".$brand."<br>"
                    ."Primary Colour: ". $pc."<br>"
                    ."Secondary Colour: ".$sc."<br>"
                    ."Power Plant: ".$pp."<br>"
                    ."Armaments: ".$arm."<br>"
                    ."Special Armaments: ".$sArm."<br>"
                    ."Special Systems/Features: ".$sSF."<br>"
                    ."Size: ".$scale."<br>"
                    ."</div><br>";
            }
        }
?>

