<?php
session_start();
include_once('../layout/page.php');
    head("../Stylesheet/style.css","Browse Auctions");
    top();
    nav();
?>



<form action='addAuction.php' method='post' class='CreateForm'>
   Brand: 
   <select name='brand'>
      <option value='Bandai' name='Bandai'>Bandai</option>
      <option value='Hasegawa' name='Hasegawa'>Hasegawa</option>
      <option value='KOTOBUKIYA' name='KOTOBUKIYA'>KOTOBUKIYA</option>
      <option value='Tamiya' name='Tamiya'>Tamiya</option>
      <option value='Unbranded' name='Unbranded'>Unbranded</option>
   </select>
   <br>
   Primary Colour: 
   <select name='primaryColour'>
      <option value='Black' name='Black'>Black</option>
      <option value='White' name='White'>White</option>
      <option value='Red' name='Red'>Red</option>
      <option value='Blue' name='Blue'>Blue</option>
      <option value='Grey' name='Grey'>Grey</option>
      <option value='Yellow' name='Yellow'>Yellow</option>
      <option value='Green' name='Green'>Green</option>
      <option value='Orange' name='Orange'>Orange</option>
   </select>
   <br>
   Secondary Colour: 
   <select name='secondaryColour'>
      <option value='Black' name='Black'>Black</option>
      <option value='White' name='White'>White</option>
      <option value='Red' name='Red'>Red</option>
      <option value='Blue' name='Blue'>Blue</option>
      <option value='Grey' name='Grey'>Grey</option>
      <option value='Yellow' name='Yellow'>Yellow</option>
      <option value='Green' name='Green'>Green</option>
      <option value='Orange' name='Orange'>Orange</option>
   </select>
   <br>
   Power Plant: 
   <select name='powerPlant'>
      <option value='Ahab Reactor' name='Ahab Reactor'>Ahab Reactor</option>
      <option value='Minovsky Ultracompact Fusion Reactor' name='Minovsky Ultracompact Fusion Reactor'>Minovsky Ultracompact Fusion Reactor</option>
      <option value='Ultracompact Energy Battery' name='Ultracompact Energy Battery'>Ultracompact Energy Battery</option>
      <option value='Solar Energy System' name='Solar Energy System'>Solar Energy System</option>
      <option value='GN Drive Tau' name='GN Drive Tau'>GN Drive Tau</option>
   </select>
   <br>
   Armaments: 
   <select name='armaments1'>
      <option value='Arondight Beam Sword'>Arondight Beam Sword</option>
      <option value='Beam Rifle(s)'>Beam Rifle(s)</option>
      <option value='Hyper DODS Rifle(s)'>Hyper DODS Rifle(s)</option>
      <option value='Muramasa Blaster'>Muramasa Blaster</option>
      <option value='Anti-Armor Shotgun'>Anti-Armor Shotgun</option>
   </select>
   and 
   <select name='armaments2'>
      <option value='Arondight Beam Sword'>Arondight Beam Sword</option>
      <option value='Beam Rifle(s)'>Beam Rifle(s)</option>
      <option value='Hyper DODS Rifle(s)'>Hyper DODS Rifle(s)</option>
      <option value='Muramasa Blaster'>Muramasa Blaster</option>
      <option value='Anti-Armor Shotgun'>Anti-Armor Shotgun</option>
   </select>
   <br>
   Special Armaments: 
   <select name='specialArmaments'>
      <option value='Rocket Anchor' name='Rocket Anchor'>Rocket Anchor</option>
      <option value='Funnels' name='Funnels'>Funnels</option>
      <option value='Beam Shield' name='Beam Shield'>Beam Shield</option>
      <option value='Torso-Mounted Missile Launcher' name='Torso-Mounted Missile Launcher'>Torso-Mounted Missile Launcher</option>
      <option value='Beam Boomerangs' name='Beam Boomerangs'>Beam Boomerangs</option>
      <option value='Absorb Shield' name='Absorb Shield'>Absorb Shield</option>
      <option value='None' name='None'>None</option>
   </select>
   <br>
   Special Systems/Features: 
   <select name='specialSystemsFeatures'>
      <option value='Detachable Body Bits' name='Detachable Body Bits'>Detachable Body Bits</option>
      <option value='Zero System' name='Zero System'>Zero System</option>
      <option value='Psycoframe Cockpit' name='Psycoframe Cockpit'>Psycoframe Cockpit</option>
      <option value='Hyper Mode Capability' name='Hyper Mode Capability'>Hyper Mode Capability</option>
      <option value='Alaya vijnana system' name='Alaya vijnana system'>Alaya vijnana system</option>
      <option value='Bio-Computer System' name='Bio-Computer System'>Bio-Computer System</option>
   </select>
   <br>
   Scale: 
   <select name='scale'>
      <option value='1:6' name='1:6'>1:6</option>
      <option value='1:8' name='1:8'>1:8</option>
      <option value='1:9' name='1:9'>1:9</option>
      <option value='1:10' name='1:10'>1:10</option>
      <option value='1:20' name='1:20'>1:20</option>
      <option value='1:35' name='1:35'>1:35</option>
      <option value='1:48' name='1:48'>1:48</option>
      <option value='1:72' name='1:72'>1:72</option>
      <option value='1:100' name='1:100'>1:100</option>
      <option value='1:120' name='1:120'>1:120</option>
      <option value='1:200' name='1:200'>1:200</option>
      <option value='1:500' name='1:500'>1:500</option>
   </select>
   <br>
   <button type='Submit' value='Submit'/>Add Auction</button>
</form>