<?php

session_start();
if (!isset($_SESSION['username'])) {
    header('location:../login.php');
}
include_once '../conn.php';

$brand = $_POST['brand'];
$PC    = $_POST['primaryColour'];
$SC    = $_POST['secondaryColour'];
$PP    = $_POST['powerPlant'];
$Arm1  = $_POST['armaments1'];
$Arm2  = $_POST['armaments2'];
$SA    = $_POST['specialArmaments'];
$SSF   = $_POST['specialSystemsFeatures'];
$SCALE = $_POST['scale'];


$dt = $_SESSION['username'].'Bids';

$S = "INSERT INTO `$dt`(`brand`, `user_id`, `primary_colour`, `secondary_colour`, `power_plant`, `armaments`, `special_armaments`, `special_systems_features`, `scale`) 
    VALUES ('" . $brand . "','User','" . $PC . "','" . $SC . "','" . $PP . "','" . $Arm1 . "','" . $SA . "','" . $SSF . "','" . $SCALE . "')";


$sql = "INSERT INTO `ActiveBids`(`brand`, `user_id`, `primary_colour`, `secondary_colour`, `power_plant`, `armaments`, `special_armaments`, `special_systems_features`, `scale`, `BidderID`, `CurrentBid`) 
    VALUES ('" . $brand . "','User','" . $PC . "','" . $SC . "','" . $PP . "','" . $Arm1 . "','" . $SA . "','" . $SSF . "','" . $SCALE . "','" . $_SESSION['username'] . "','" . 0 . "')";

if ($conn->query($sql) === TRUE && $conn->query($S) === TRUE ) {
    echo "New record created successfully";
    header("location:UserBrowse.php?=SuccessfullyAddedAuction");
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

?>