<?php
/*Everything in HTML head Section*/
function head($stylesheet, $title){
    echo "<head>
   <meta charset=\"UTF-8\">
   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
   <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
       <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
   <link rel=\"stylesheet\" href=\"$stylesheet\">
   <title>$title</title>
   </head>
   ";
}




/*Navigation template */
function nav(){
    echo"
    <nav class='navigationTabs'>
        <a href='UserBrowse.php'>Browse Auctions</a>
        <a href='UserCreateAuction.php'>Create Auctions</a>
        <a href='UserAuction.php'>Active Auctions</a>
        <a href='../logout.php'>Logout</a>
    </nav>

    
    ";
}

function AdminNav(){
    echo"
    <nav class='navigationTabs'>
    <a href='Admin.php'>Users</a>
    <a href='AdminUserAuctionList.php'>User Auctions</a>
    <a href='../logout.php'>Logout</a>
    </nav>
    ";
}

/*Top template */
function top(){
    echo"
    <div>
        <h1 class='top' id='pageTitle'>Gundam_Auction_V48</h1>
    </div>
    ";
}

?>