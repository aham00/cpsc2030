<?php

session_start();

if (!isset($_SESSION['username'])) {
    header('location:../login.php');
}

include_once '../layout/page.php';
include_once '../conn.php';
head("../Stylesheet/style.css","Admin Page");
top();
AdminNav();

$sql = "SELECT * FROM Users;";
$result = mysqli_query($conn, $sql);
$resultCheck = mysqli_num_rows($result);

echo "
    <div class='UserList'>
    <div class='allusers'>
        All Users:<br>
    </div>
    ";
    
if ($resultCheck > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $user     = $row['Username'];
        $password = $row['Password'];
        $hash     = password_hash($password, PASSWORD_DEFAULT);
        
        echo "
                <div class='users'>
                Username: $user<br>
                Password: $hash<br>
                </div>
                ";
    }
    echo"</div>";
}
?>
<div class="userForms">
    <form action="signup.php" method="POST" class="signupbox">
        <input type="text" name="Username" placeholder="Username">
        <br>
        <input type="text" name="Password" placeholder="Password">
        <br>
        <button type="submit" name="submit">Add User</button>
    </form>
    
    
    <form action="deleteUser.php" method="POST" class="deletebox">
        <input type="text" name="Username" placeholder="Username">
        <br>
        <button type="submit" name="submit">Delete User</button>
    </form>
</div>

