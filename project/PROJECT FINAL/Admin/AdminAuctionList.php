<?php
session_start();
if (!isset($_SESSION['username'])) {
    header('location:../login.php');
}
include_once '../layout/page.php';
include_once '../conn.php';
head("../Stylesheet/style.css", "Auction Page(Admin)");
top();
AdminNav();



$sql         = "SELECT * FROM ActiveBids;";
$result      = mysqli_query($conn, $sql);
$resultCheck = mysqli_num_rows($result);
echo "<div class='flex_cont'> ";

if ($resultCheck > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $id    = $row['mech_id'];
        $brand = $row['brand'];
        $pc    = $row['primary_colour'];
        $sc    = $row['secondary_colour'];
        $pp    = $row['power_plant'];
        $arm   = $row['armaments'];
        $sArm  = $row['special_armaments'];
        $sSF   = $row['special_systems_features'];
        $scale = $row['scale'];
        
        echo "<div class='bidbox'>" . "Auction ID #: " . $id . "<br>" . "Brand: " . $brand . "<br>" . "Primary Colour: " . $pc . "<br>" . "Secondary Colour: " . $sc . "<br>" . "Power Plant: " . $pp . "<br>" . "Armaments: " . $arm . "<br>" . "Special Armaments: " . $sArm . "<br>" . "Special Systems/Features: " . $sSF . "<br>" . "Size: " . $scale . "<br>" . "</div><br>";
        
    }
}


echo"<form action='deleteauction.php' method='post'>
        <input type='text' name='auctionID' placeholder='Auction ID'>
        <br>
        <button type='submit' name='deleteauction'>Delete Auction</input>
    </form></div>";