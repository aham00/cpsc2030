<?php
    include_once 'conn.php';
    include_once 'Layout/page.php';
    head("style/style2.css","Active Auctions");
    top();
    nav();
?>




<body>
    <?php
        $sql = "SELECT * FROM ActiveBids;";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        echo "<div class='flex_cont'> ";
        
        if($resultCheck > 0){
            while($row = mysqli_fetch_assoc($result)){
                $id = $row['mech_id'];
                $brand = $row['brand'];
                $pc = $row['primary_colour'];
                $sc = $row['secondary_colour'];
                $pp = $row['power_plant'];
                $arm = $row['armaments'];
                $sArm = $row['special_armaments'];
                $sSF = $row['special_systems_features'];
                $scale = $row['scale'];
                
                echo "<div class='bidbox'>"
                    ."Brand: ".$brand."<br>"
                    ."Primary Colour: ". $pc."<br>"
                    ."Secondary Colour: ".$sc."<br>"
                    ."Power Plant: ".$pp."<br>"
                    ."Armaments: ".$arm."<br>"
                    ."Special Armaments: ".$sArm."<br>"
                    ."Special Systems/Features: ".$sSF."<br>"
                    ."Size: ".$scale."<br>"
                    ."<form action='ActiveAuctions.php' method='post'>
                    <button type='submit' name='getBidButton'>Bid</input>
                    </form>"
                    ."</div>";
                
                
            }
        }
        echo"</div>";
    ?>
</body>
</html>