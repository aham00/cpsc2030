<?php



function head($stylesheet, $script, $title){
    echo "<head>
   <meta charset=\"UTF-8\">
   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
   <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
   <link rel=\"stylesheet\" href=\"$stylesheet\">
   <script src=\"$script\" defer></script>
   <title>$title</title>
   </head>";
};


function title(){
    echo "<h1 id='headMain'>
        Inefficient PHP Game
        </h1>";
};


function score($score){
    echo "<p id='score'>
    Score:$score
    </p>";
};





function controls(){
    echo "
    <div class='container'>
        <button id='upKey' onclick='up()'>Up</button>
        <button id='leftKey' onkeypress='left()'>Left</button>
        <button id='downKey' onkeypress='down()'>Down</button>
        <button id='rightKey' onkeypress='right()'>Right</button>
        <button id='reset' onkeypress='reset()'>Reset</button>
    </div>
    ";
};




?>