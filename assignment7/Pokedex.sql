create database Pokedex;

create table Pokemon (

    Nat.# INT;
    Name Text;
    URL text;
    Type text;
    HP INT;
    Atk INT;
    Def INT;
    SAt INT;
    SDf INT;
    spd INT;
    BST INT;
    

);

create table Effective (
    
    Type Text;
    Strong Against Text;
    Weak Against Text;
    Resistant To Text;
    Vulnerable To Text;

);

INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("NORMAL","","ROCK/GHOST/STEEL","GHOST","FIGHTING");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("FIGHTING","NORMAL/ROCK/STEEL/ICE/DARK","FLYING/POISON/PSYCHC/BUG/GHOST/FAIRY","ROCK/BUG/DARK","FLYING/PSYCHC/FAIRY");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("FLYING","FIGHTING/BUG/GRASS","ROCK/STEEL/ELECTRIC","FIGHTING/GROUND/BUG/GRASS","ROCK/PSYCHC/ICE");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("POISON","GRASS/FAIRY","POISON/GROUND/ROCK/GHOST/STEEL","FIGHTING/POISON/GRASS/FAIRY","GROUND/PSYCHC");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("GROUND","POISON/ROCK/STEEL/FIRE/ELECTRIC","FLYING/BUG/GRASS","POISON/ROCK/ELECTRIC","WATER/GRASS/ICE");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("ROCK","FLYING/BUG/FIRE/ICE","FIGHTING/GROUND/STEEL","NORMAL/FLYING/POISON/FIRE","FIGHTING/GROUND/STEEL/WATER/GRASS");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("BUG","GRASS/PSYCHC/DARK","FIGHTING/FLYING/POISON/GHOST/STEEL/FIRE/FAIRY","FIGHTING/GROUND/GRASS","FLYING/ROCK/FIRE");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("GHOST","GHOST/PSYCHC","NORMAL/DARK","NORMAL/FIGHTING/POISON/BUG","GHOST/DARK");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("STEEL","ROCK/ICE/FAIRY","STEEL/FIRE/WATER/ELECTRIC","NORMAL/FLYING/POISON/ROCK/BUG/STEEL/GRASS/PSYCHC/ICE/DRAGON/FAIRY","FIGHTING/GROUND/FIRE");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("FIRE","BUG/STEEL/GRASS/ICE","ROCK/FIRE/WATER/DRAGON","BUG/STEEL/FIRE/GRASS/ICE","GROUND/ROCK/WATER");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("WATER","GROUND/ROCK/FIRE","WATER/GRASS/DRAGON","STEEL/FIRE/WATER/ICE","GRASS/ELECTRIC");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("GRASS","GROUND/ROCK/WATER","FLYING/POISON/BUG/STEEL/FIRE/GRASS/DRAGON","GROUND/WATER/GRASS/ELECTRIC","FLYING/POISON/BUG/FIRE/ICE");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("ELECTRIC","FLYING/WATER","GROUND/GRASS/ELECTRIC/DRAGON","FLYING/STEEL/ELECTRIC","GROUND");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("PSYCHC","FIGHTING/POISON","STEEL/PSYCHC/DARK","FIGHTING/PSYCHC","BUG/GHOST/DARK");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("ICE","FLYING/GROUND/GRASS/DRAGON","STEEL/FIRE/WATER/ICE","ICE","FIGHTING/ROCK/STEEL/FIRE");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("DRAGON","DRAGON","STEEL/FAIRY","FIRE/WATER/GRASS/ELECTIC","ICE/DRAGON/FAIRY");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("FAIRY","FIGHTING/DRAGON/DARK","POISON/STEEL/FIRE","FIGHTING/BUG/DRAGON/DARK","POISON/STEEL");
INSERT INTO `Effective`(`Type`, `Strong Against`, `Weak against`, `Resistant To`, `Vulnerable To`) VALUES ("DARK","GHOST/PSYCHC","FIGHTING/DARK/FAIRY","GHOST/PSYCHC/DARK","FIGHTING/BUG/FAIRY");




INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("1","Bulbasaur","https://media-cerulean.cursecdn.com/avatars/320/585/1.png.png","GRASS/POISON","45","49","49","65","65","45","318");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("2","Ivysaur","","GRASS/POISON","60","62","63","80","80","60","405");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("3","Venusaur","","GRASS/POISON","80","82","83","100","100","80","525",);
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("4","Charmander","","FIRE","39","52","43","60","50","65","309");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("5","Charmeleon","","FIRE","58","64","58","80","65","80","405");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("6","Charizard","","FIRE/FLYING","78","84","78","109","85","100","534");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("7","Squirtle","","WATER","44","48","65","50","64","43","314");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("8","Wartortle","","WATER","59","63","80","65","80","58","405");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("9","Blastoise","","WATER","79","83","100","85","105","78","530");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("10","Caterpie","","BUG","45","30","35","20","20","45","195");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("11","Metapod","","BUG","50","20","55","25","25","30","205");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("12","Butterfree","","BUG/FLYING","60","45","50","90","80","70","295");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("13","Weedle","","BUG/POISON","40","35","30","20","20","50","195");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("14","Kakuna","","BUG/POISON","45","25","50","25","25","35","205");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("15","Beedrill","","BUG/POISON","65","90","40","45","80","75","395");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("16","Pidgey","","NORMAL/FLYING","40","45","40","35","35","56","251");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("17","Pidgeotto","","NORMAL/FLYING","63","60","55","50","50","71","349");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("18","Pidgeot","","NORMAL/FLYING","83","80","75","70","70","101","479");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("19","Rattata","","NORMAL","30","56","35","25","35","72","253");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("20","Raticate","","NORMAL","55","81","60","50","70","97","413");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("21","Spearow","","NORMAL/FLYING","40","60","30","31","31","70","262");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("22","Fearow","","NORMAL/FLYING","65","90","65","61","61","100","442");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("23","Ekans","","POISON","35","60","44","40","54","55","288");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("24","Arbok","","POISON","60","85","69","65","79","80","438");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("25","Pikachu","","ELECTR","35","55","40","50","50","90","320");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("26","Raichu","","ELECTR","60","90","55","90","80","110","485");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("27","Sandshrew","","GROUND","50","75","85","20","30","40","300");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("28","Sandslash","","GROUND","75","100","110","45","55","65","450");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("29","Nidoran♀","","POISON","55","47","52","40","40","41","275");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("30","Nidorina","","POISON","70","62","67","55","55","56","365");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("31","Nidoqueen","","POISON/GROUND","90","92","87","75","85","76","505");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("32","Nidoran♂","","POISON","46","57","40","40","40","50","273");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("33","Nidorino","","POISON","61","72","57","55","55","65","365");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("34","Nidoking","","POISON/GROUND","81","102","77","85","75","85","505");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("35","Clefairy","","FAIRY","70","45","48","60","65","35","323");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("36","Clefable","","FAIRY","95","70","73","95","90","60","483");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("37","Vulpix","","FIRE","38","41","40","50","65","65","299");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("38","Nintales","","FIRE","73","76","75","81","100","100","505");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("39","Jigglypuff","","NORMAL/FAIRY","70","45","48","60","65","35","323");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("40","Wigglypuff","","NORMAL/FAIRY","70","45","48","60","65","35","323");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("41","Zubat","","POISON/FLYING","40","45","35","30","40","55","245");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("42","Golbat","","POISON/FLYING","75","80","70","65","75","90","455");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("43","Oddish","","GRASS/POISON","45","50","55","75","65","30","320");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("44","Gloom","","GRASS/POISON","60","65","70","85","75","40","395");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("45","Vileplume","","GRASS/POISON","75","80","85","110","90","50","490");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("46","Paras","","BUG/GRASS","","35","70","55","45","55","25","285");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("47","Parasect","","BUG/GRASS","60","95","80","60","80","30","405");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("48","Venonat","","BUG/POISON","70","45","48","60","65","35","305");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("49","Venomoth","","BUG/POISON","70","65","60","90","75","90","450");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("50","Diglett","","GROUND","10","55","25","35","45","95","265");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("51","Dugtrio","","GROUND","35","80","50","50","70","120","405");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("52","Meowth","","NORMAL","40","45","35","40","40","90","290");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("53","Persian","","NORMAL","65","70","60","65","65","115","440");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("54","Psyduck","","WATER","50","52","48","65","50","55","320");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("55","Golduck","","WATER","80","82","78","95","80","85","500");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("56","Mankey","","FIGHTING","40","80","35","35","45","70","305");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("57","Primeape","","POISON/FLYING","70","45","48","60","65","35","305");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("58","Growlithe","","FIRE","55","70","45","70","50","60","350");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("59","Arcanine","","FIRE","90","110","80","100","80","95","555");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("60","Poliwag","","WATER","40","50","40","40","40","90","300");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("61","Poliwhirl","","WATER","65","65","65","50","50","90","385");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("62","Poliwrath","","WATER/FIGHTING","90","95","95","70","90","70","510");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("63","Abra","","PSYCHC","25","20","15","105","55","90","310");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("64","Kadabra","","PSYCHC","40","35","30","120","70","105","400");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("65","Alakazam","","PSYCHC","55","50","45","135","95","120","500");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("66","Machop","","FIGHTING","70","80","50","35","35","35","305");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("67","Machoke","","FIGHTING","80","100","70","50","60","45","405");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("68","Machamp","","FIGHTING","90","130","80","65","85","55","505");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("69","Bellsprout","","GRASS/POISON","50","75","35","70","30","40","300");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("70","Weepinbell","","GRASS/POISON","65","90","50","85","45","55","390");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("71","Victreebel","","GRASS/POISON","80","105","65","100","70","70","490");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("72","Tentacool","","WATER/POISON","40","40","35","50","100","70","335");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("73","Tentacruel","","WATER/POISON","80","70","65","80","120","100","515");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("74","Geodude","","ROCK/GROUND","40","80","100","30","30","20","300");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("75","Graveler","","ROCK/GROUND","55","95","115","45","45","35","390");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("76","Golem","","ROCK/GROUND","80","120","130","55","65","65","495");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("77","Ponyta","","FIRE","50","85","55","65","65","90","410");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("78","Rapidash","","FIRE","65","100","70","80","80","105","500");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("79","Slowpoke","","WATER/PSYCHC","90","65","65","40","40","15","315");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("80","Slowbro","","WATER/PSYCHC","95","75","110","100","80","30","490");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("81","Magnemite","","POISON/FLYING","25","35","70","95","55","45","325");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("82","Magneton","","Magenton","50","60","95","120","70","70","465");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("83","Farfetch'd","","NORMAL/FLYING","52","65","55","58","62","60","352");
INSERT INTO `Pokemon`(`Nat.#`, `Name`, `URL`, `Type`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `spd`, `BST`) VALUES ("84","Doduo","","NORMAL/FLYING","35","85","45","35","35","35","310");






