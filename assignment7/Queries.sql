/*Delete Pokedex database*/
DROP DATABASE Pokedex;

/* Top 10 Pokemon by Defense */
SELECT `Nat.#`, Name, Def
FROM Pokemon
ORDER BY Def DESC
LIMIT 10;


/* Pokemon Vulnerable to Fighting Strong against Ice */
SELECT Pokemon.Name
FROM Pokemon
WHERE Pokemon.Type LIKE '%ROCK%' || '%STEEL%';

/*Pokemon BST between 300-400 strong against Flying weak against Grass */

SELECT Pokemon.Name
FROM Pokemon
Where Pokemon.Type LIKE '%ELECTR%' && Pokemon.BST BETWEEN 300 AND 400;

