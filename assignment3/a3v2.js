let apiURL = 'https://pokeapi.co/api/v2/pokemon-species';
let LoSn = document.getElementById('LoSn');
let details = document.getElementById('sDet');
let previousURL = "";


function NS() {
    LoSn.innerHTML = "";
    details.innerHTML = "";

    fetch(apiURL)
        .then(response => {
            return response.json()
        })
        .then(data => {
            let speciesCounter = 0;
            data.results.forEach(function(element) {
                let name = data.results[speciesCounter].name;
                let pokeurl = data.results[speciesCounter].url;
                let para = document.createElement('p');
                para.onclick = function() {
                    fetch(pokeurl)
                        .then(response => {
                            return response.json()
                        })
                        .then(data => {
                            let happy = data.base_happiness;
                            let cRate = data.capture_rate;
                            let flavor = data.flavor_text_entries.length - 10;
                            details.innerHTML = (`${data.flavor_text_entries[flavor].flavor_text}
                            Base Happiness = ${happy}
                            Capture Rate = ${cRate}`);
                        });
                };
                let pokename = document.createTextNode(name);
                LoSn.appendChild(para);
                para.appendChild(pokename);
                para.classList.add("indexname");
                speciesCounter++;
            });
            apiURL = data.next;
            previousURL = (data.previous);
            console.log(previousURL)
        });
};



function PS() {
    LoSn.innerHTML = "";
    details.innerHTML = "";



    fetch(previousURL)
        .then(response => {
            return response.json()
        })
        .then(data => {

            let speciesCounter = 0;
            data.results.forEach(function(element) {

                let name = data.results[speciesCounter].name;
                let pokeurl = data.results[speciesCounter].url;

                let para = document.createElement('p');
                para.onclick = function() {
                    fetch(pokeurl)
                        .then(response => {
                            return response.json()
                        })
                        .then(data => {
                            let happy = data.base_happiness;
                            let cRate = data.capture_rate;
                            let flavor = data.flavor_text_entries.length - 10;
                            details.innerHTML = (`${data.flavor_text_entries[flavor].flavor_text}
                            Base Happiness = ${happy}
                            Capture Rate = ${cRate}`);
                        })

                }


                let pokename = document.createTextNode(name);
                LoSn.appendChild(para);
                para.appendChild(pokename);
                para.className += 'indexname';

                speciesCounter++;
            })




            apiURL = data.next;
            previousURL = data.previous;
            console.log(previousURL)
        })



}