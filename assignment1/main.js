let valueArray = [];

function addArrayItem(i, r, x, y, picString) {
    valueArray.push({
        i: i,
        r: r,
        x: x,
        y: y,
        picString: picString
    });
}

function getLatest() {
    return valueArray[valueArray.length - 1]
}

function reset() {
    location.reload();
}

function loopbody() {
    if (valueArray.length < 1) {
        getValue();
    } else {
        let lastStackFrame = getLatest();
        let i = lastStackFrame.i;
        let N = document.getElementById("N").value;
        let r1 = document.getElementById("r1").value;
        let r2 = document.getElementById("r2").value;
        let M = 4;
        let k = eval(document.getElementById("k").value);
        let r = lastStackFrame.r;
        let x = lastStackFrame.x;
        let y = lastStackFrame.y;
        let picString = lastStackFrame.picString + " ";
        let svgString;
        if (i < k) {
            i++;
            switch (i % M) {
                case 0:
                case 1:
                    r = r1;
                    break;
                case 2:
                case 3:
                    r = r2;
                    break;
            };

            let x = r * Math.cos(i * 2 * Math.PI / (N * M)) + parseInt(document.getElementById("Math1").value);
            let y = r * Math.sin(i * 2 * Math.PI / (N * M)) + parseInt(document.getElementById("Math1").value);
            picString = picString + `${x},${y}`;
            svgString = `<polygon points="${picString}" stroke="black"/ >`;
            document.querySelector("#output").innerHTML = svgString;


            let TT = document.getElementById("TraceTable");
            let TTR = document.createElement("tr");
            TT.appendChild(TTR);

            let TTD1 = document.createElement("td");
            let TI = document.createTextNode(i);
            TTD1.appendChild(TI);
            let TTD2 = document.createElement("td");
            let TR = document.createTextNode(r);
            TTD2.appendChild(TR);
            let TTD3 = document.createElement("td");
            let TX = document.createTextNode(x);
            TTD3.appendChild(TX);
            let TTD4 = document.createElement("td");
            let TY = document.createTextNode(y);
            TTD4.appendChild(TY);
            let TTD5 = document.createElement("td");
            let TP = document.createTextNode(picString);
            TTD5.appendChild(TP);
            TTR.appendChild(TTD1);
            TTR.appendChild(TTD2);
            TTR.appendChild(TTD3);
            TTR.appendChild(TTD4);
            TTR.appendChild(TTD5);

            addArrayItem(i, r, x, y, picString);
        } else {
            console.log("You have reached the end of the loop!");
        }
    }
}

function getValue() {
    let picString ="";
    let svgString ="";
    let r = 0;
    let M = 4;
    let N = document.getElementById("N").value;
    let r1 = document.getElementById("r1").value;
    let r2 = document.getElementById("r2").value;
    let k = eval(document.getElementById("k").value);
    let i = document.getElementById("i").value;
    let z = parseInt(document.getElementById("Math1").value);
    let j = parseInt(document.getElementById("Math2").value);
    
    
    switch (i % k) {
        case 0:
        case 1:
            r = r1;
            break;
        case 2:
        case 3:
            r = r2;
            break;
    };
    
    
    let x = r * Math.cos(i * 2 * Math.PI / (N * M)) + z;
    let y = r * Math.sin(i * 2 * Math.PI / (N * M)) + j;
    picString = picString + `${x},${y}`;
    svgString = `<polygon points="${picString}" stroke="black"/ >`;
    document.querySelector("#output").innerHTML = svgString;



    let TT = document.getElementById("TraceTable");
    let TTR = document.createElement("tr");
    TT.appendChild(TTR);
    let TTD1 = document.createElement("td");
    let TI = document.createTextNode(i);
    TTD1.appendChild(TI);
    let TTD2 = document.createElement("td");
    let TR = document.createTextNode(r);
    TTD2.appendChild(TR);
    let TTD3 = document.createElement("td");
    let TX = document.createTextNode(x);
    TTD3.appendChild(TX);
    let TTD4 = document.createElement("td");
    let TY = document.createTextNode(y);
    TTD4.appendChild(TY);
    let TTD5 = document.createElement("td");
    let TP = document.createTextNode(picString);
    TTD5.appendChild(TP);
    TTR.appendChild(TTD1);
    TTR.appendChild(TTD2);
    TTR.appendChild(TTD3);
    TTR.appendChild(TTD4);
    TTR.appendChild(TTD5);

    addArrayItem(i, r, x, y, picString);
    i++;
}


function stepBack() {
    let svgString;
    let table = document.getElementById("TraceTable");
    let row = table.rows.length;
    if (row == 1) {
        console.log("The loop hasn't started yet");
    } else {
        table.deleteRow(row - 1);
        valueArray.pop();
        let lastStackFrame = getLatest();
        let picString = lastStackFrame.picString;
        svgString = `<polygon points="${picString}" stroke="black"/ >`;
        document.querySelector("#output").innerHTML = svgString;
    }



}